FROM scratch
MAINTAINER Martin Beranek (martin.beranek112@gmail.com)

COPY timeout_restarter /timeout_restarter
CMD ["/timeout_restarter"]
