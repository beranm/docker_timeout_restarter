# Docker Timeout Restarter

It's a very small binary which takes a look at the running containers and restarts those which are older than CONTAINER_TIMEOUT. The list of containers which should be restarted is at CONTAINER_NAME_LIST. The items are divided by `,` and items are regexes which should match the containers.


### Example

```
docker run -ti -e DOCKER_API_VERSION="v1.40" -e CONTAINER_TIMEOUT=10 -e CONTAINER_NAME_LIST=".*traefik.*" -v /var/run/docker.sock:/var/run/docker.sock beranm14/docker_timeout_restarter /timeout_restarter
```