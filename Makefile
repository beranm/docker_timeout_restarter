timeout_restarter:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go get github.com/docker/docker/api/types
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go get github.com/docker/docker/client
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o timeout_restarter timeout_restarter.go/main.go

