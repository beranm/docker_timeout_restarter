package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func main() {
	var timeout *time.Duration
	timeoutValue := time.Duration(60) * time.Second
	timeout = &timeoutValue

	containerNameListPtr := flag.String("containerNameList", "", "Containers names as regexes to be restarted at given time")

	var containerNameList string
	if os.Getenv("CONTAINER_NAME_LIST") != "" {
		containerNameList = os.Getenv("CONTAINER_NAME_LIST")
	} else {
		containerNameList = *containerNameListPtr
	}
	containerTimeoutPtr := flag.Int("containerTimeout", 86400, "Container time to be alive in seconds, if reached, container is restarted")
	var containerTimeout int
	if os.Getenv("CONTAINER_TIMEOUT") != "" {
		containerTimeout, _ = strconv.Atoi(os.Getenv("CONTAINER_TIMEOUT"))
	} else {
		containerTimeout = *containerTimeoutPtr
	}

	containerRegNames := strings.Split(containerNameList, ",")
	if len(containerRegNames) == 0 || containerNameList == "" {
		fmt.Println("No regexes in container name list")
		os.Exit(2)
	}
	if containerTimeout == 0 {
		fmt.Println("Tmeout for the container should be bigger than 0")
		os.Exit(2)
	}
	for {
		ctx := context.Background()
		cli, err := client.NewClientWithOpts(client.FromEnv)
		if err != nil {
			panic(err)
		}
		cli.NegotiateAPIVersion(ctx)
		containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
		if err != nil {
			panic(err)
		}

		for _, container := range containers {
			var containerName *string
			for _, name := range container.Names {
				if name[0] == '/' {
					name = name[1:]
				}
				containerName = &name
			}
			for _, ri := range containerRegNames {
				r, _ := regexp.Compile(ri)
				if r.MatchString(*containerName) {
					containerInspect, _ := cli.ContainerInspect(ctx, container.ID)
					t1, _ := time.Parse(time.RFC3339, containerInspect.State.StartedAt)
					t2 := time.Now()
					alive := t2.Sub(t1).Seconds()
					if int(alive) > containerTimeout {
						fmt.Println("Restarting ", *containerName, " older than ", containerTimeout)
						cli.ContainerRestart(context.Background(), container.ID, timeout)
					}
				}
			}
		}
		fmt.Println("Check executed, waiting")
		time.Sleep(60 * time.Second)
	}
}
